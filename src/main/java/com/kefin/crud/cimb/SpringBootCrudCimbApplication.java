package com.kefin.crud.cimb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCrudCimbApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCrudCimbApplication.class, args);
	}

}
