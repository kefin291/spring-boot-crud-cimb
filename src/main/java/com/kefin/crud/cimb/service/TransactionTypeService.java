package com.kefin.crud.cimb.service;

import com.kefin.crud.cimb.entity.TransactionType;
import com.kefin.crud.cimb.repository.TransactionTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionTypeService {
    @Autowired
    private TransactionTypeRepository repository;

    public TransactionType saveTransactionType(TransactionType transactionType) {
        return repository.save(transactionType);
    }

    public List<TransactionType> saveTransactionTypes(List<TransactionType> transactionTypes) {
        return repository.saveAll(transactionTypes);
    }

    public List<TransactionType> getTransactionTypes() {
        return repository.findAll();
    }

    public TransactionType getTransactionTypeById(int id) {
        return repository.findById(id).orElse(null);
    }

    public TransactionType getTransactionTypeByName(String name) {
        return repository.findByTransactionName(name);
    }

    public String deleteTransactionType(int id) {
        repository.deleteById(id);
        return "transaction type removed !! " + id;
    }

    public TransactionType updateTransactionType(TransactionType transactionType) {
        TransactionType existingTransactionType = repository.findById(transactionType.getTransaction_type_id()).orElse(null);
        existingTransactionType.setTransaction_type_id(transactionType.getTransaction_type_id());
        existingTransactionType.setTransaction_code(transactionType.getTransaction_code());
        existingTransactionType.setTransaction_name(transactionType.getTransaction_name());
        return repository.save(existingTransactionType);
    }


}
