package com.kefin.crud.cimb.service;

import com.kefin.crud.cimb.entity.TransactionHistory;
import com.kefin.crud.cimb.repository.TransactionHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class TransactionHistoryService {
    @Autowired
    private TransactionHistoryRepository repository;

    public TransactionHistory saveTransactionHistory(TransactionHistory TransactionHistory) {
        return repository.save(TransactionHistory);
    }

    public List<TransactionHistory> saveTransactionHistorys(List<TransactionHistory> TransactionHistorys) {
        return repository.saveAll(TransactionHistorys);
    }

    public List<TransactionHistory> getTransactionHistorys() {
        return repository.findAll();
    }

    public TransactionHistory getTransactionHistoryById(int id) {
        return repository.findById(id).orElse(null);
    }

    public String deleteTransactionHistory(int id) {
        repository.deleteByTrxHisId(id);
        return "TransactionHistory removed !! " + id;
    }

    public TransactionHistory updateTransactionHistory(TransactionHistory TransactionHistory) {
        TransactionHistory existingTransactionHistory = repository.findById(TransactionHistory.getTransaction_history_id()).orElse(null);
        existingTransactionHistory.setTransaction_history_id(TransactionHistory.getTransaction_history_id());
        existingTransactionHistory.setActivity_date(new Timestamp(System.currentTimeMillis()));
        existingTransactionHistory.setAmount(TransactionHistory.getAmount());
        existingTransactionHistory.setUser(TransactionHistory.getUser());
        existingTransactionHistory.setTransactionType(TransactionHistory.getTransactionType());
        return repository.save(existingTransactionHistory);
    }

}
