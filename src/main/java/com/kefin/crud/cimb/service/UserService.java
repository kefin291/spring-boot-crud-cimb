package com.kefin.crud.cimb.service;

import com.kefin.crud.cimb.entity.User;
import com.kefin.crud.cimb.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository repository;

    public User saveUser(User user) {
        return repository.save(user);
    }

    public List<User> saveUsers(List<User> users) {
        return repository.saveAll(users);
    }

    public List<User> getUsers() {
        return repository.findAll();
    }

    public User getUserById(int id) {
        return repository.findById(id).orElse(null);
    }

    public User getUserByName(String name) {
        return repository.findByUserName(name);
    }

    public String deleteUser(int id) {
        repository.deleteById(id);
        return "user removed !! " + id;
    }

    public User updateUser(User user) {
        User existingUser = repository.findById(user.getUser_id()).orElse(null);
        existingUser.setUsername(user.getUsername());
        existingUser.setPassword_hash(user.getPassword_hash());
        existingUser.setAccount_number(user.getAccount_number());
        return repository.save(existingUser);
    }


}
