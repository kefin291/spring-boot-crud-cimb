package com.kefin.crud.cimb.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "USER")
public class User {

    @Id
    @GeneratedValue
    private int user_id;
    private String username;
    private String password_hash;
    private String account_number;
}
