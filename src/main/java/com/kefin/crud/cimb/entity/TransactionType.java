package com.kefin.crud.cimb.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "TRX_TYP")
public class TransactionType {

    @Id
    @GeneratedValue
    private int transaction_type_id;
    private String transaction_code;
    private String transaction_name;
}
