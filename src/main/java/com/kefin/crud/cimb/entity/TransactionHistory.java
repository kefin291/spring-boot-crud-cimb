package com.kefin.crud.cimb.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "TRX_HISTORY")
public class TransactionHistory {

    @Id
    @GeneratedValue
    private int transaction_history_id;
    private Timestamp activity_date;
    private int amount;
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private User user;
    @ManyToOne
    @JoinColumn(name = "transaction_type_id", referencedColumnName = "transaction_type_id")
    private TransactionType transactionType;
}
