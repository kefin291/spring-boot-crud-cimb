package com.kefin.crud.cimb.repository;

import com.kefin.crud.cimb.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User,Integer> {
    @Query("select u from User u where u.username=: n")
    User findByUserName(String name);
}

