package com.kefin.crud.cimb.repository;

import com.kefin.crud.cimb.entity.TransactionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TransactionTypeRepository extends JpaRepository<TransactionType,Integer> {
    @Query(value = "select * from trx_typ tt where tt.transaction_name=:name",nativeQuery = true)
    TransactionType findByTransactionName(@Param("name") String name);
}

