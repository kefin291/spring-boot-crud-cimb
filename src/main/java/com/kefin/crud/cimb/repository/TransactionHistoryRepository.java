package com.kefin.crud.cimb.repository;

import com.kefin.crud.cimb.entity.TransactionHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory,Integer> {

    @Modifying
    @Transactional
    @Query(value ="delete from trx_history where transaction_history_id=:id",nativeQuery = true)
    void deleteByTrxHisId(@Param("id") int id);
}

