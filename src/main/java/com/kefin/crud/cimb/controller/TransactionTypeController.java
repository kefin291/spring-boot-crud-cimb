package com.kefin.crud.cimb.controller;

import com.kefin.crud.cimb.entity.TransactionType;
import com.kefin.crud.cimb.service.TransactionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TransactionTypeController {

    @Autowired
    private TransactionTypeService service;

    @PostMapping("/addTransactionType")
    public TransactionType addTransactionType(@RequestBody TransactionType TransactionType) {
        return service.saveTransactionType(TransactionType);
    }

    @PostMapping("/addTransactionTypes")
    public List<TransactionType> addTransactionTypes(@RequestBody List<TransactionType> TransactionTypes) {
        return service.saveTransactionTypes(TransactionTypes);
    }

    @GetMapping("/transactionTypes")
    public List<TransactionType> findAllTransactionTypes() {
        return service.getTransactionTypes();
    }

    @GetMapping("/transactionTypeById/{id}")
    public TransactionType findTransactionTypeById(@PathVariable int id) {
        return service.getTransactionTypeById(id);
    }

    @GetMapping("/transactionType/{name}")
    public TransactionType findTransactionTypeByName(@PathVariable String name) {
        return service.getTransactionTypeByName(name);
    }

    @PutMapping("/updateTransactionType")
    public TransactionType updateTransactionType(@RequestBody TransactionType TransactionType) {
        return service.updateTransactionType(TransactionType);
    }

    @DeleteMapping("/deleteTransactionType/{id}")
    public String deleteTransactionType(@PathVariable int id) {
        return service.deleteTransactionType(id);
    }
}
