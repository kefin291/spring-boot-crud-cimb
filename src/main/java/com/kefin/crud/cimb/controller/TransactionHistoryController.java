package com.kefin.crud.cimb.controller;

import com.kefin.crud.cimb.entity.TransactionHistory;
import com.kefin.crud.cimb.service.TransactionHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TransactionHistoryController {

    @Autowired
    private TransactionHistoryService service;

    @PostMapping("/addTransactionHistory")
    public TransactionHistory addTransactionHistory(@RequestBody TransactionHistory TransactionHistory) {
        return service.saveTransactionHistory(TransactionHistory);
    }

    @PostMapping("/addTransactionHistorys")
    public List<TransactionHistory> addTransactionHistorys(@RequestBody List<TransactionHistory> TransactionHistorys) {
        return service.saveTransactionHistorys(TransactionHistorys);
    }

    @GetMapping("/transactionHistorys")
    public List<TransactionHistory> findAllTransactionHistorys() {
        return service.getTransactionHistorys();
    }

    @GetMapping("/transactionHistoryById/{id}")
    public TransactionHistory findTransactionHistoryById(@PathVariable int id) {
        return service.getTransactionHistoryById(id);
    }

    @PutMapping("/updateTransactionHistory")
    public TransactionHistory updateTransactionHistory(@RequestBody TransactionHistory TransactionHistory) {
        return service.updateTransactionHistory(TransactionHistory);
    }

    @DeleteMapping("/deleteTransactionHistory/{id}")
    public String deleteTransactionHistory(@PathVariable int id) {
        return service.deleteTransactionHistory(id);
    }
}
